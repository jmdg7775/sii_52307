// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	centro.x=2.5;
	centro.y=2.5;
	radio=0.5f;
	velocidad.x=7;
	velocidad.y=0;
	//posicion.x=centro.x;
	//posicion.y=centro.y;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glTranslatef(-centro.x,-centro.y,0);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
centro.y=centro.y+velocidad.y*t;
centro.x=centro.x+velocidad.x*t;
}
