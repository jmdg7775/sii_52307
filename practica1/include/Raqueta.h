// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	Vector2D velocidad,aceleracion;
	Raqueta();
	bool Rebote (Esfera &e, Raqueta r);
	virtual ~Raqueta();

	void Mueve(float t);
};
